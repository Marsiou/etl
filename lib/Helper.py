import numpy
from pyproj import Proj
from shapely.geometry import shape
import re
import unicodedata
from pprint import pprint

class Calc:
	def CalcPolygonAreaHectare(self, poly):
		lon, lat = zip(*poly)
		pa = Proj("+proj=aea +ellps=WGS84")
		
		x, y = pa(lon, lat)
		cop = {"type": "Polygon", "coordinates": [zip(x, y)]}
		
		return shape(cop).area
	def reproject(self,latitude, longitude):
		"""Returns the x & y coordinates in meters using a sinusoidal projection"""
		from math import pi, cos, radians
		earth_radius = 6371009 # in meters
		lat_dist = pi * earth_radius / 180.0

		y = [lat * lat_dist for lat in latitude]
		x = [long * lat_dist * cos(radians(lat)) 
					for lat, long in zip(latitude, longitude)]
		return x, y
	def area_of_polygon(self,x, y):
		"""Calculates the area of an arbitrary polygon given its verticies"""
		area = 0.0
		for i in range(-1, len(x)-1):
			area += x[i] * (y[i+1] - y[i-1])
		return abs(area) / 2.0
	def CalcPolygonCentroid(self, polygon, area):
		"""Calculate the centroid of non-self-intersecting polygon
		Input
			polygon: Numeric array of points (longitude, latitude). It is assumed
					 to be closed, i.e. first and last points are identical
		Output
			Numeric (1 x 2) array of points representing the centroid
		"""

		# Make sure it is numeric
		P = numpy.array(polygon)

		# Get area - needed to compute centroid
		

		# Extract x and y coordinates
		x = P[:, 0]
		y = P[:, 1]

		# Exercise: Compute C as shown in http://paulbourke.net/geometry/polyarea
		a = x[:-1] * y[1:]
		b = y[:-1] * x[1:]

		cx = x[:-1] + x[1:]
		cy = y[:-1] + y[1:]

		Cx = numpy.sum(cx * (a - b)) / (6. * area)
		Cy = numpy.sum(cy * (a - b)) / (6. * area)

		# Create Nx2 array and return
		#C = numpy.array([Cx, Cy])
		
		return [Cx, Cy]
	def CalcPolygonArea(self, polygon, signed=False):
		"""Calculate the signed area of non-self-intersecting polygon
		Input
			polygon: Numeric array of points (longitude, latitude). It is assumed
					 to be closed, i.e. first and last points are identical
			signed: Optional flag deciding whether returned area retains its sign:
					If points are ordered counter clockwise, the signed area
					will be positive.
					If points are ordered clockwise, it will be negative
					Default is False which means that the area is always positive.
		Output
			area: Area of polygon (subject to the value of argument signed)
		"""

		# Make sure it is numeric
		P = numpy.array(polygon)

		# Check input
		msg = ('Polygon is assumed to consist of coordinate pairs. '
			   'I got second dimension %i instead of 2' % P.shape[1])
		assert P.shape[1] == 2, msg

		msg = ('Polygon is assumed to be closed. '
			   'However first and last coordinates are different: '
			   '(%f, %f) and (%f, %f)' % (P[0, 0], P[0, 1], P[-1, 0], P[-1, 1]))
		assert numpy.allclose(P[0, :], P[-1, :]), msg

		# Extract x and y coordinates
		x = P[:, 0]
		y = P[:, 1]

		# Area calculation
		a = x[:-1] * y[1:]
		b = y[:-1] * x[1:]
		A = numpy.sum(a - b) / 2.

		# Return signed or unsigned area
		if signed:
			return A
		else:
			return abs(A)

class MatchData:
	@staticmethod
	def FindValuesInArray(collections, valueToMatch, singleValue):
		valFind = []
		valueToMatchNorm = MatchData.NormalizeString(valueToMatch)
		for collection in collections:
			for variety in collection["variety"]:				
				
				if len(re.findall('\\b' +MatchData.NormalizeString(variety)+'\\b', valueToMatchNorm, flags=re.IGNORECASE)) > 0 or MatchData.NormalizeString(variety) == valueToMatchNorm :
					#print('FIND ['+MatchData.NormalizeString(variety) + ']  ['+valueToMatchNorm+']')
					valFind.append(collection["_id"])
					if singleValue :
						return valFind
					break
		return valFind
	def FindValues(collections, valueToMatch, singleValue):
		valFind = []
		valueToMatchNorm = MatchData.NormalizeString(valueToMatch)
		for arr in collections:
				variety = arr["name"]
				if len(re.findall('\\b' +MatchData.NormalizeString(variety)+'\\b', valueToMatchNorm, flags=re.IGNORECASE)) > 0 or MatchData.NormalizeString(variety) == valueToMatchNorm :
					#print('FIND ['+MatchData.NormalizeString(variety) + ']  ['+valueToMatchNorm+']')
					valFind.append(arr["id"])
					if singleValue :
						return valFind
		return valFind
	@staticmethod
	def StartWith(collections, valueToMatch):
		valueToMatchNorm = MatchData.NormalizeString(valueToMatch)
		for name in collections:
			nameNorm = MatchData.NormalizeString(name)
			if valueToMatchNorm.startswith(nameNorm) == True:
				print("valueToMatchNorm "+valueToMatchNorm + "   nameNorm" +nameNorm)
				return True
		return False
		
	def FindValuesContainInArray(collections, valueToMatch, singleValue):
		valFind = []
		valueToMatchNorm = MatchData.NormalizeString(valueToMatch)
		for collection in collections:
			for variety in collection["variety"]:				
				
				if MatchData.NormalizeString(variety) in valueToMatchNorm :
					#print('FIND ['+MatchData.NormalizeString(variety) + ']  ['+valueToMatchNorm+']')
					valFind.append(collection["_id"])
					if singleValue :
						return valFind
					break
		return valFind
	@staticmethod
	def NormalizeString(input_str):
		return ''.join((c for c in unicodedata.normalize('NFD', input_str) if unicodedata.category(c) != 'Mn')).lower().strip()
