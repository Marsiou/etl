# -*- coding: utf-8 -*-
import json
from pprint import pprint
from lxml import html, etree
import io
import requests
from xml.etree.ElementTree import ElementTree, XML, fromstring, tostring
import unicodedata
import re
from . import HelperDB, Collections, Helper
from html.parser import HTMLParser
import time
import urllib 
import hashlib
import sys, locale, os 
from PIL import Image
import codecs


class Scraping:
	def ExtractFishs(self):
		page = requests.get('https://fr.wikipedia.org/wiki/Liste_de_poissons')
		parser = etree.HTMLParser()
		tree = etree.parse(io.StringIO(page.text), parser=parser).xpath('//*[@id="mw-content-text"]/*')
		jsonFish = []
		count = 1
		for elem in tree:
			findId = 0
			for x in list(elem.iter('span')):
				if x.attrib.get('id') == 'Voir_aussi':
					findId = 1
			if findId == 1:
				break
			if elem.tag == 'ul':
				for x in list(elem.iter('li')):
					count2 = 0
					jsonFishSingle = {}
					for u in list(x.iter('a')):
						if count2 == 0:
							jsonFishSingle['_id'] = count
							jsonFishSingle['commonName'] =u.text
							jsonFishSingle['url'] ='https://fr.wikipedia.org' + u.attrib.get('href')
							jsonFishSingle['asciiName'] = Helper.MatchData.NormalizeString(u.text)
						else:
							jsonFishSingle['scientificName'] = u.text
							jsonFishSingle['urlScientific'] ='https://fr.wikipedia.org' +u.attrib.get('href')
							break
						count2 += 1
					count += 1
					if count2 > 0:
						jsonFish.append(jsonFishSingle)
		mongo = HelperDB.Mongo()
		mongo.Remove("fishs",{})	
		mongo.BulkInsert( 'fishs', jsonFish)
	def ExtractRiviere(self):
		page = requests.get('https://fr.wikipedia.org/wiki/Liste_de_rivi%C3%A8res_de_France')
		parser = etree.HTMLParser()
		tree = etree.parse(io.StringIO(page.text), parser=parser).xpath('/descendant::li')
		jsonRivieres=[]	
		counterScrap = 0;
		for li in tree:
			json = {}
			if li.attrib.get('class') == None:
				for u in list(li.iter('a')):
					json['name'] =u.text
					json['variety'] =[]
					json['variety'].append(Helper.MatchData.NormalizeString(u.text))
					json['url'] = 'https://fr.wikipedia.org' + u.attrib.get('href')
					print('START=> '+str(counterScrap) + ' url=>'+json['url'])
					page2 = requests.get(json['url'])
					sandre = etree.parse(io.StringIO(page2.text), parser=parser).xpath('//*[contains(translate(@id, "ABCDEFGHJIKLMNOPQRSTUVWXYZ", "abcdefghjiklmnopqrstuvwxyz"), "cite_note-sandre-")]/span[2]//*[@class="external text"]')
					if len(sandre) == 0:
						sandre = etree.parse(io.StringIO(page2.text), parser=parser).xpath('//*[translate(@id, "ABCDEFGHJIKLMNOPQRSTUVWXYZ", "abcdefghjiklmnopqrstuvwxyz")="sandre"]//*[@class="external text"]')
					if len(sandre) > 0:
						sandreText = sandre[0].attrib.get('href')
						if sandreText != None:
							json['refSandre'] = sandreText[sandreText.find("=")+1:len(sandreText)]
							print(json['refSandre'] +"=>"+sandreText)
					
					tree2 = etree.parse(io.StringIO(page2.text), parser=parser).xpath('//*[@class="infobox_v2"]//tr[2]/td[1]')
					if len(tree2) != 0:
						baliseImage = tree2[0].find('a')
						json['image'] = {}
						json['image']["allResolution"] = 'https://fr.wikipedia.org' + baliseImage.attrib.get('href')
						json['image']["description"] = tree2[0].xpath("string()").strip()
						json['image']["thumbnail"] = 'https:' + baliseImage.find('img').attrib.get('src')
						json['image']["name"] =""
						if not "Defaut.svg" in json['image']["thumbnail"] and not "Defaut_2.svg" in json['image']["thumbnail"]:
							page3 = requests.get(json['image']["allResolution"])
							parser3 = etree.HTMLParser()
							tree3= etree.parse(io.StringIO(page3.text), parser=parser).xpath('//*[@class="fullImageLink"]/a')#'//a[contains(@href, "1024px")]')
							if len(tree3) != 0:
								ert = Scraping.DownloadImg('https:' + tree3[0].attrib.get('href') ,'/home/webuser/oupecher/public/img/tmp/spots/normal/', None)
								json['image']["name"] = Scraping.DownloadImg(json['image']['thumbnail'],'/home/webuser/oupecher/public/img/tmp/spots/thumbnail/', ert)
							else:
								print ('NO IMAGE =>' + json['image']["allResolution"])
					else:
							print ('NO INFOBOX =>' +json['url'])
					jsonRivieres.append(json)
					#time.sleep(2)
					break;
				print('END  => '+str(counterScrap) + ' url=>'+json['url'])
				counterScrap +=1
				if json['name'] == 'Zorn':
					break;
		mongo = HelperDB.Mongo()
		mongo.Remove("WikiRivieres2",{})	
		mongo.BulkInsert( 'WikiRivieres2', jsonRivieres)
	def ExtractFleuve2(self):
		page = requests.get('https://fr.wikipedia.org/wiki/Liste_des_fleuves_de_France')
		parser = etree.HTMLParser()
		tree = etree.parse(io.StringIO(page.text), parser=parser).xpath('/descendant::table[1]//td[1]')
		jsonFleuves=[]	
		counterScrap = 0;
		for elem in tree:
			json = {}
			for u in list(elem.iter('a')):	
				#remove anchor
				if u.attrib.get('href').find("#") == -1:
					json['name'] =u.text
					json['variety'] =[]
					json['variety'].append(Helper.MatchData.NormalizeString(u.text))
					json['url'] ='https://fr.wikipedia.org' + u.attrib.get('href')
					print('START=> '+str(counterScrap) + ' url=>'+json['url'])
					page2 = requests.get(json['url'])
					sandre = etree.parse(io.StringIO(page2.text), parser=parser).xpath('//*[contains(translate(@id, "ABCDEFGHJIKLMNOPQRSTUVWXYZ", "abcdefghjiklmnopqrstuvwxyz"), "cite_note-sandre-")]/span[2]//*[@class="external text"]')
					if len(sandre) == 0:
						sandre = etree.parse(io.StringIO(page2.text), parser=parser).xpath('//*[translate(@id, "ABCDEFGHJIKLMNOPQRSTUVWXYZ", "abcdefghjiklmnopqrstuvwxyz")="sandre"]//*[@class="external text"]')
					if len(sandre) > 0:
						sandreText = sandre[0].attrib.get('href')
						if sandreText != None:
							json['refSandre'] = sandreText[sandreText.find("=")+1:len(sandreText)]
							print(json['refSandre'] +"=>"+sandreText)
					
					tree2 = etree.parse(io.StringIO(page2.text), parser=parser).xpath('//*[@class="infobox_v2"]//tr[2]/td[1]')
					if len(tree2) != 0:
						baliseImage = tree2[0].find('a')
						json['image'] = {}
						json['image']["allResolution"] = 'https://fr.wikipedia.org' + baliseImage.attrib.get('href')
						json['image']["description"] = tree2[0].xpath("string()").strip()
						json['image']["thumbnail"] = 'https:' + baliseImage.find('img').attrib.get('src')
						json['image']["name"] =""
						if not "Defaut.svg" in json['image']["thumbnail"] and not "Defaut_2.svg" in json['image']["thumbnail"]:
							page3 = requests.get(json['image']["allResolution"])
							parser3 = etree.HTMLParser()
							tree3= etree.parse(io.StringIO(page3.text), parser=parser).xpath('//*[@class="fullImageLink"]/a')#'//a[contains(@href, "1024px")]')
							if len(tree3) != 0:
								ert = Scraping.DownloadImg('https:' + tree3[0].attrib.get('href') ,'/home/webuser/oupecher/public/img/tmp/spots/normal/', None)
								json['image']["name"] = Scraping.DownloadImg(json['image']['thumbnail'],'/home/webuser/oupecher/public/img/tmp/spots/thumbnail/', ert)
							else:
								print ('NO IMAGE =>' + json['image']["allResolution"])
					else:
							print ('NO INFOBOX =>' +json['url'])
					jsonFleuves.append(json)
					#time.sleep(2)
					break;
				print('END  => '+str(counterScrap) + ' url=>'+json['url'])
				counterScrap +=1
				if json['name'] == 'Zorn':
					break;
		mongo = HelperDB.Mongo()
		mongo.Remove("WikiFleuves",{})	
		mongo.BulkInsert( 'WikiFleuves', jsonFleuves)
	def ExtractGobages(self):
		mongo = HelperDB.Mongo()
		parser = etree.HTMLParser()
		uniqid = mongo.GetSpotMaxUniqId("stagingSpots")
		collection = Collections.Collections()
		fishs = list(mongo.Find('fishs', {}))
		types =list(mongo.Find("waterTypes", {}))
		numberFind = 0
		pageM = requests.get('http://www.gobages.com/parcours/reservoir/page/1')
		idLastPage = etree.parse(io.StringIO(pageM.text), parser=parser).xpath('//*[@class="last"]')[0].text
		pageNumber = 1
		while pageNumber <= int(idLastPage):
			print(pageNumber)
			page = requests.get('http://www.gobages.com/parcours/reservoir/page/' + str(pageNumber))
			linkInsidePage = etree.parse(io.StringIO(page.text), parser=parser).xpath('//*[@class="td-module-image"]//div[@class="td-module-thumb"]/a/@href')
			for urlSpot in linkInsidePage:
				getSingleSpot = requests.get(urlSpot)
				contentPage = etree.parse(io.StringIO(getSingleSpot.text), parser=parser).xpath('//*[@class="meta-parcours-header-contenus"]/p//text()')
				time.sleep(3)
				res = [txt.strip() for txt in contentPage if txt.strip()]
				count = 0
				print(urlSpot)
				spot = collection.Spot()
				findType = False
				spot["geometry"] = { "type" : "Point", "coordinates" : [ 0, 0 ] }
				for i in range(len(res)):
					if res[i] == 'Rivière :' or res[i] == 'Réservoir :':
						for type in types:
							for variety in type["variety"]:
								var =Helper.MatchData.NormalizeString(variety)
								if var in res[i]:
									spot["properties"]["type"] = type["_id"]
									findType = True
									break
							if findType == True:
								break
						spot["properties"]["source"] = "Gobage"
						if findType == False:
							spot["properties"]["type"] = [type["_id"] for type in types if type["name"] == "Inconnu"]
						
						spot["properties"]["toponyme"] = res[i+1]
						spot["properties"]["name"] = res[i+1]
					elif res[i] == 'Lattitude :':
						spot["geometry"]["coordinates"][1] = float(res[i+1])
					elif res[i] == 'Longitude :':
						spot["geometry"]["coordinates"][0] = float(res[i+1])
					elif res[i] == 'Population du parcours :':
						strfishs=Helper.MatchData.NormalizeString(res[i+1])
						fishFind = [fishSingle["_id"] for fishSingle in fishs if fishSingle["asciiName"]in strfishs ]
						spot["properties"]["fishs"] = fishFind
					elif res[i] == 'Tarifs :':
						spot["properties"]["rules"]["restrictions"].append("payant")
					elif res[i] == 'Superficie :':
						#Recupere que les float et les digites dans la chaine de charatere
						spot["properties"]["size"]=re.findall(r"[-+]?\d*\.\d+|\d+", res[i+1])
					elif res[i] == 'Site Web :':
						contact = collection.Contact()
						contact["webSite"] = res[i+1]
						_idContact =[]
						if contact["webSite"] or contact["adresse"] or contact["phoneNumber"] or contact["mail"] :
							_idContact = mongo.Insert("contacts", contact)
						spot["properties"]["contacts"].append(_idContact)
				findDepartement = mongo.Find('departements', { 'geometry' : { '$geoIntersects': { '$geometry':spot['geometry'] }}})
				spot["properties"]["departements"] = [dep["_id"] for dep in findDepartement]
				findExist =list(mongo.Find("newSpots2",{ 'geometry' : { '$geoIntersects': { '$geometry':spot['geometry'] }}}))
				if len(findExist) > 0 and len(spot["properties"]["fishs"]) > 0:
					numberFind+=1
					query = {"_id" : findExist[0]["_id"]} 
					data = { '$set':
								{
								"properties.fishs" : list(set(spot["properties"]["fishs"]+ findExist[0]["properties"]["fishs"])),
								"properties.rules.restrictions" : list(set(spot["properties"]["rules"]["restrictions"]+ findExist[0]["properties"]["rules"]["restrictions"]))
								}
							}
					mongo.Update("newSpots2", query, data)
				# else:
					# spot["properties"]["uniqid"] = uniqid
					# uniqid+=1
					# mongo.Insert('stagingSpots', spot)
			pageNumber +=1
		print("Total update"+str(numberFind))
	def ExtractAchigan(self):
		mongo = HelperDB.Mongo()
		mongo.Remove("dataArchigan",{})
		mysql = HelperDB.MySql()
		restrictions = list(mysql.Find("SELECT * FROM Restrictions"))
		fishs = list(mysql.Find("SELECT * FROM Fishes"))
		categories = list(mongo.Find("categories",{}))
		accordRecipro = list(mysql.Find("SELECT * FROM Restrictions WHERE name IN ('CHI','EHGO','URNE')"))
		#Code to execute in the page achigan and C/C in file lst_spots
		#for(var i = 0; i < 2000 ; ++i) if(window['markerSpot' + i] != undefined) console.log(i + ',' + window['markerSpot' + i].title +','+ window['markerSpot' + i].position.lat()+','+ window['markerSpot' + i].position.lng())
		achiganCoord = []
		file = open('/home/martial/DataToImport/etl/lst_spots.txt', 'r')
		for line in file:
			line = line.rstrip().split(',')
			achiganCoord.append({"id":line[0], "name":line[1], "coordinates":{"lon" : line[2],"lat" : line[3]}})
		parser = etree.HTMLParser()
		for milieu in ["lac", "cours"]:
			page = requests.get('http://www.achigan.net/spot.php?milieu='+ milieu)
			getLastPage = etree.parse(io.StringIO(page.text), parser=parser).xpath('//*[@class="pages"]//*[@class="soustexte2"]/a[last()]')
			lastPageId = getLastPage[0].text
			for pageNumber in range(1, int(lastPageId)):
				page2 = requests.get('http://www.achigan.net/spot.php?milieu='+ milieu+'&page='+str(pageNumber))
				contentTable = etree.parse(io.StringIO(page2.text), parser=parser).xpath('//*[@class="tabspots"]//tr')
				for tr in contentTable:
					spot ={}
					for td in tr.xpath(".//td"):
						if td.get('class') == 'tabspots_cat' :
							colContent = td.xpath("string()")
							spot["category"] = Helper.MatchData.FindValuesInArray(categories, colContent, False)
							colImg = td.xpath(".//img")
							if len(colImg) > 0:
								srcImg = colImg[0].get('src')
								if "money" in srcImg:
									spot["restrictions"] = [restriction["id"] for restriction in restrictions if restriction["asciiName"] == "payant"]
								elif "stamp" in srcImg:
									spot["restrictions"] = [restriction["id"] for restriction in restrictions if restriction["asciiName"] == "timbre supplementaire"]
								elif "lock" in srcImg:
									spot["restrictions"] = [restriction["id"] for restriction in restrictions if restriction["asciiName"] == "payant"]
								elif "forbidden" in srcImg:
									spot["restrictions"] = [restriction["id"] for restriction in restrictions if restriction["asciiName"] == "peche interdite"]
								else:
									spot["restrictions"] = [restriction["id"] for restriction in restrictions if restriction["asciiName"] == "reciprocitaire"]
						elif td.get('class') == 'tabspots_nav' :
							colImg = td.xpath(".//img")
							if len(colImg) > 0:
								spot["navigable"] = True
							else:
								spot["navigable"] = False
						elif td.get('class') == 'tabspots_nom' :
							href = td.xpath(".//a/@href")
							if len(href) > 0:
								spot["url"] = href[0]
								print(spot["url"])
								firstOccurence = spot["url"].find("-")
								spot["sourceId"] = spot["url"][firstOccurence + 1 : spot["url"].find("-", firstOccurence + 1 )]
								pageSpot = requests.get(spot["url"])
								nameSpot = etree.parse(io.StringIO(pageSpot.text), parser=parser).xpath('//*[@id="rubrique"]/h1')
								if len(nameSpot) > 0:
									spot["name"] = nameSpot[0].xpath("string()")
									if spot["name"].find('(') != -1:
										spot["name"]  = spot["name"][spot["name"].find('(') +1:len(spot["name"])]
								contentSpot = etree.parse(io.StringIO(pageSpot.text), parser=parser).xpath('//*[@id="spot"]')
								if len(contentSpot) > 0:
									strContent = contentSpot[0].xpath("string()")
									spot["fishs"] = Helper.MatchData.FindValues(fishs, strContent, False)
									idGeo = [coord["coordinates"] for coord in achiganCoord if coord["id"] == spot["sourceId"]]
									if len(idGeo) > 0:
										spot["geometry"] = { "type" : "Point", "coordinates": [float(idGeo[0]['lat']), float(idGeo[0]['lon'])]}
									accordReciproFind =  Helper.MatchData.FindValues(accordRecipro, strContent, False)
									if len(accordReciproFind)> 0:
										spot["restrictions"] = list(set(spot["restrictions"] + accordReciproFind))
								time.sleep(5)
								 # Helper.MatchData.FindValuesInArray(categories, colContent, False)
								# spot["category"] = Helper.MatchData.FindValuesInArray(categories, colContent, False)
					pprint(spot)
					if "name" in spot:
						mongo.Insert("dataArchigan", spot)
			print("\n")
	@staticmethod
	def DownloadImg(url, path, newName): 
		url = url
		filename = ''
		if newName != None:
			filename = newName
		else:
			filename = urllib.parse.unquote(url.split('/')[-1])
		fileExtension = '.' + filename.split('.')[-1]
		if fileExtension.upper() != '.PDF':
			pathFile = path+filename
			response = requests.get(url)
			if response.status_code == 200:
				f = open(pathFile, 'wb')
				f.write(response.content)
				f.close()
			if newName == None:
				im = Image.open(pathFile)
				newFileName = hashlib.md5(im.tobytes()).hexdigest() +fileExtension
				os.rename(pathFile, path+newFileName)
				return newFileName
		return newName