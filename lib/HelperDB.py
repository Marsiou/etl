# -*- coding: utf-8 -*-
from pymongo import MongoClient
from pymongo.errors import AutoReconnect
import MySQLdb as db

def autoreconnect_retry(retries=3):
	def autoreconnect_retry_decorator(fn):
		def db_op_wrapper(*args, **kwargs):
			tries = 0
			while tries < retries:
				try:
					return fn(*args, **kwargs)
				except AutoReconnect:
					tries += 1
			raise Exception("No luck even after %d retries" % retries)
		return db_op_wrapper
	return autoreconnect_retry_decorator

class Mongo:
	def OpenConnection(self):
		client = MongoClient('localhost', 27017, connectTimeoutMS=10000)
		#client.the_database.authenticate('adminfish','12345678', source='fishingdb')
		return client
	def UseCollection(self, client, collectionName):
		return client['fishingdb'][collectionName]
	@autoreconnect_retry(4)
	def Find(self, collectionName, filters):
		client = self.OpenConnection()
		dbCollection = self.UseCollection(client,collectionName)
		dataJson = dbCollection.find(filters)
		client.close()
		pass
		return dataJson
	@autoreconnect_retry(4)
	def FindOne(self, collectionName, filters):
		client = self.OpenConnection()
		dbCollection = self.UseCollection(client,collectionName)
		dataJson = dbCollection.find_one(filters)
		client.close()
		pass
		return dataJson
	@autoreconnect_retry(4)
	def GetSpotMaxUniqId(self, collectionName):
		client = self.OpenConnection()
		dbCollection = self.UseCollection(client, collectionName)
		dataJson = dbCollection.find_one({"$and" :[ {"properties.uniqid":{"$exists": True}}, {"properties.uniqid":{"$ne":""}}]}, {"properties.uniqid":1}, sort=[("properties.uniqid", -1)])
		if dataJson == None:
			dataJson = 1
		else:
			try: 
				dataJson = int(dataJson["properties"]["uniqid"]) + 1
			except ValueError:
				dataJson = 1
		client.close()
		pass
		return dataJson
	@autoreconnect_retry(4)
	def FindBatch(self, collectionName, filters, batchSize):
		client = self.OpenConnection()
		dbCollection = self.UseCollection(client,collectionName)
		dataJson = dbCollection.find(filters).batch_size(batchSize)
		client.close()
		pass
		return dataJson
	@autoreconnect_retry(4)
	def RemoveInsert(self, collectionName, documentJSON):
		client = self.OpenConnection()
		dbCollection = self.UseCollection(client,collectionName)
		dbCollection.remove()
		dbCollection.insert(documentJSON)
		client.close()
		pass
	@autoreconnect_retry(4)	
	def Remove(self, collectionName, filter):
		client = self.OpenConnection()
		dbCollection = self.UseCollection(client,collectionName)
		dbCollection.remove(filter)
		client.close()
		pass
	@autoreconnect_retry(4)
	def Insert(self, collectionName, documentJSON):
		client = self.OpenConnection()
		dbCollection = self.UseCollection(client,collectionName)
		_id = dbCollection.save(documentJSON)
		client.close()
		pass
		return _id
	@autoreconnect_retry(4)
	def Update(self, collectionName, filter, documentJSON):
		client = self.OpenConnection()
		dbCollection = self.UseCollection(client,collectionName)
		dbCollection.update(filter, documentJSON)
		client.close()
		pass
	@autoreconnect_retry(4)
	def BulkInsert(self, collectionName, arrayJson):
		client = self.OpenConnection()
		dbCollection = self.UseCollection(client,collectionName)
		bulk = dbCollection.initialize_unordered_bulk_op()
		for doc in arrayJson:
			bulk.insert(doc)
		bulk.execute()
		client.close()
		pass
	

class MySql:
	def OpenConnection(self):
		#client =  db.connect('localhost', 'root', 'ipsos49a', 'oupecher');
		client =  db.connect('localhost', 'root', 'ipsos49a', 'oupecher-dev');
		return client
	def Find(self, query):
		client = self.OpenConnection()
		cur = client.cursor(db.cursors.DictCursor)
		cur.execute(query)
		data = cur.fetchall()
		client.close()
		return data
	def FindRow(self, query):
		client = self.OpenConnection()
		cur = client.cursor()
		cur.execute(query)
		data = cur.fetchall()
		client.close()
		return data