import datetime
import json

class Collections:
	def ES_Spot(self):
		return {
			"type": {"type": "String"}, 
				"properties": {
						"uniqid":{"type": "integer"},
						"sourceId":{"type": "string"}, 
						"source" : {"type": "string"}, 
						"refSandre":{"type": "string"}, 
						"name": {"type": "string"},
						"toponyme" : {"type": "string"}, 
						"category": {"type":"Array", "es_type":"integer"},
						"size": {"type":"Array", "es_type":"integer"},
						"departements": {"type":"Array", "es_type":"string"}, 
						"classification": {"type":"Array", "es_type":"integer"},
						"fishs" : {"type":"Array", "es_type":"integer"},
						"centroid" : {
							"coordinates": {"type": "Array"}, 
							"geo_shape": {
							"es_type": "geo_shape",
							"es_tree": "quadtree",
							"es_precision": "100km"
							}
						},
						"description" : {"type": "string"},
						"isPublic" :{"type": "boolean"},
						"isValide" : {"type": "boolean"},
						"navigable" : {"type": "string"}, 
						"isPublic": {"type": "boolean"},
						"rules": {
						 "allowedFishingTech": {"type":"Array", "es_type":"integer"},
						 "forbidenFishingTech": {"type":"Array", "es_type":"integer"},
						 "restrictions": {"type":"Array", "es_type":"integer"},
						},
						"createdDate" : {"type": "date"}, 
						"updatedDate" : {"type": "date"}
				},
				"geometry": {
				 "properties": {
					"coordinates": {"type": "Array"}, 
					"geo_shape": {
					"es_type": "geo_shape",
					"es_tree": "quadtree",
					"es_precision": "100km"
					}
					}
				}
		}
	def ES_aggrSpot(self):
		return{
				"name": {"type": "string"}, 
				"description" :{"type": "string"}, 
				"size":{"type": "string"}, 
				"refSandre":{"type": "string"}, 
				"osmId":{"type": "string"}, 
				"type":{"type": "string"}, 
				"boat":{"type": "string"}, 
				"spots":{"type":"string", },
				"createdDate" : {"type": "date"},
				"updatedDate" : {"type": "date"}
			 }
	@staticmethod
	def Spot():
		return {"type": "Feature", 
				"properties": 
					{
					"uniqid" :"",
					"enable" :True,
					"sourceId": "", 
					"source" : "",
					"wikipediaUrl" : "",
					"refSandre":"",
					"name": "", 
					"toponyme" : "",
					"classification": [], 
					"departements" :[],
					"fishs" : [],
					"size" : [],
					"centroid" : { "coordinates" : [ ] } ,
					"description" : "",
					"isPublic" : True,
					"category" : [],
					"isValide" : False,
					"navigable" : False,
					"contacts" : [],
					"slipway" : [],
					"rules" : { "restrictions" : [], "allowedFishingTech" :[], "forbidenFishingTech" : [] },
					"createdDate" : datetime.datetime.utcnow(),
					"updatedDate" : datetime.datetime.utcnow()
					},
				"geometry" : "" }
	def aggrSpot(self):
		return{
				"name":"",
				"description" : "",
				"size":"",
				"refSandre":"",
				"osmId":"",
				"type":"",
				"boat":"",
				"spots":[],
				"createdDate" : datetime.datetime.utcnow(),
				"updatedDate" : datetime.datetime.utcnow()
			 }
	def Contact(self):
		return {
				"webSite" : "",
				"adress" : "",
				"phoneNumber" : "",
				"mail" : ""}
	@staticmethod
	def TransformSandreCours(self, Cours):
		spot = self.Spot()
		spot["properties"]["sourceId"] = str(Cours["properties"]["Code_Hydrographique"])
		spot["properties"]["refSandre"] = str(Cours["properties"]["Code_Hydrographique"])
		spot["properties"]["source"] = "SandreCours"
		spot["properties"]["name"] = Cours["properties"]["Toponyme"]
		spot["properties"]["toponyme"] = Cours["properties"]["Toponyme"]
		spot["geometry"]= Cours['geometry']
		return spot
	@staticmethod
	def TransformSandrePlan(self, Cours):
		spot = self.Spot()
		spot["properties"]["sourceId"] = str(Cours["properties"]["CdEniteHydrographique"])
		spot["properties"]["refSandre"] = str(Cours["properties"]["CdEniteHydrographique"])
		spot["properties"]["name"] = str(Cours["properties"]["NomEntiteHydrographique"])
		spot["properties"]["toponyme"] =  str(Cours["properties"]["NomEntiteHydrographique"])
		spot["properties"]["source"] = "SandrePlanEau"
		spot["geometry"]= Cours['geometry']
		return spot
	@staticmethod
	def TransformSandreSurface(self, Cours):
		spot = self.Spot()
		spot["properties"]["sourceId"] = str(Cours["properties"]["ID_BDCARTH"])
		spot["properties"]["refSandre"] = str(Cours["properties"]["ID_BDCARTH"])
		spot["properties"]["source"] = "SandreSurface"
		spot["properties"]["name"] = Cours["properties"]["TOPONYME"]
		spot["properties"]["toponyme"] = Cours["properties"]["TOPONYME"]
		spot["geometry"]= Cours['geometry']
		return spot
	@staticmethod
	def TransformOSMRiver(self, river):
		spot = self.Spot()
		spot["properties"]["sourceId"] = river[0]
		spot["properties"]["source"] = "OSM"
		spot["properties"]["name"] = river[1]
		spot["properties"]["toponyme"] = river[1]
		spot["properties"]["refSandre"] = river[2]
		spot["properties"]["navigable"] = river[4]
		spot["geometry"] = json.loads(river[5])
		spot["properties"]["centroid"]["coordinates"] = [{"lon" : coord[0], "lat" : coord[1]} for coord in spot["geometry"]["coordinates"]]
		return spot
	@staticmethod
	def TransformOSMLine(self, data):
		aggr = self.aggrSpot()
		aggr["osmId"] =data[0]
		aggr["name"] =data[1]
		aggr["refSandre"] =data[2]
		aggr["type"] =data[3]
		aggr["boat"] = data[4]
		return aggr

