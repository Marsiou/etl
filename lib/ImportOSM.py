import psycopg2
from pymongo import MongoClient
from pprint import pprint
from . import HelperDB, Collections, Helper
from bson.objectid import ObjectId

_CONNECTION_POSTGRES = "dbname='francev2' user='postgres' host='localhost' password='ipsos49a'"
_COLLECTION_SPOT = "spots2"
_COLLECTION_SPOT_DETAIL = "spotDetails"
class ImportOSM:
	def __init__(self):
		mongo = HelperDB.Mongo()
		print("DROP COLLECTIONS " + _COLLECTION_SPOT)
		mongo.Remove(_COLLECTION_SPOT, {})
		print("DROP COLLECTIONS " + _COLLECTION_SPOT_DETAIL)
		mongo.Remove(_COLLECTION_SPOT_DETAIL, {})
		self.MergeOSMLineData(self.GetOSMRiverFromRels())
		#InsertStream()
		#InsertCanal()
	def GetOSMRiverFromRels(self):
		print("GetOSMRiverFromRels")
		#les champs doivent etre dans le bon ordre utiliser par 'Collections.TransformOSMLine'
		query = """
			SELECT id
				, tags->'name' AS Name
				, tags->'ref:sandre' AS refsandre
				, tags->'waterway' AS typeWaterway
				, tags->'boat' AS boat
				, parts
			FROM		planet_osm_rels
			WHERE		(tags  @>'waterway=>river' ) """
		conn = psycopg2.connect(_CONNECTION_POSTGRES)
		cursor = conn.cursor()
		cursor.execute(query)
		for row in cursor:
			yield row
		cursor.close()
		conn.close()
	def MergeOSMLineData(self, iterators):
		mongo = HelperDB.Mongo()
		collection = Collections.Collections()
		counter = 0
		counterSpotDetail = 0
		for row in iterators:
			#mongo.FindOne("spots2", {"osmId"})
			aggrSpot = collection.TransformOSMLine(row)
			aggrSpot["_id"] = ObjectId()
			conn = psycopg2.connect(_CONNECTION_POSTGRES)
			cursor = conn.cursor()
			cursor.execute("""
				SELECT 	osm_id
					, ps.name 
					, tags->'ref:sandre' AS refsandre
					, tags->'waterway' AS typeWaterway
					, tags->'boat' AS boat
					, ST_AsGeoJSON(ps.way) as geojson
				FROM   planet_osm_line  ps
				WHERE ps.osm_id IN (SELECT unnest(ARRAY"""+str(row[5])+"""))
					""")
			spots = []
			spotsId = []
			for line in cursor:
				spotDetails = collection.TransformOSMRiver(line)
				spotDetails["_id"] = ObjectId()
				spotDetails["_parent"] = aggrSpot["_id"]
				spotsId.append(spotDetails["_id"])
				spots.append(spotDetails)
				counterSpotDetail =  counterSpotDetail+1
			if len(spots) > 0:
				mongo.BulkInsert(_COLLECTION_SPOT_DETAIL, spots)
			else :
				print('enter')
			counter = counter +1
			print('River '+ str(counter)+ ' ' + str(counterSpotDetail))
			aggrSpot["spots"] = spotsId
			mongo.Insert(_COLLECTION_SPOT, aggrSpot)
			cursor.close()
			conn.close()
	def FindSpot(self):
		return ""
	def FindDepartment(self):
		return ""
	
	
	
