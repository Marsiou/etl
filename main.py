# -*- coding: utf8 -*-
from pprint import pprint
from lib import * 
import sys, locale, os 
import requests
import imp 
import glob 
import ijson 
import json	
import gzip 
import time
#imp.reload(sys) sys.defaultencoding("utf-8")
from pymongo import MongoClient 
from bson.json_util import dumps 
import re 
import unicodedata 
import datetime
import copy
from bson.objectid import ObjectId
from elasticsearch import Elasticsearch
import subprocess
import time
import MySQLdb as db



		
class ETL:
	def initRefCollections(self):
		#self.LoadDepartement()
		self.LoadFishs()
		self.LoadWaterType()
		self.LoadCategory()
		self.LoadRule()
		self.LoadFishingTech()
	def LoadDepartement(self):
		mongo = HelperDB.Mongo()
		mongo.Remove('departements',{})
		data = []
		with open('../DepartementWGS84_UTF.json') as f:
			for line in f.readlines():
				try:
					js = json.loads(line)
					js["_id"] = js["properties"]["CODE_DEPT"]
					data.append(js)
				except:
					pass
			if len(data) > 0:
				mongo = HelperDB.Mongo()
				mongo.BulkInsert( 'departements', data)
	def LoadFishs(self):
		scrap = Scraping.Scraping()
		mongo = HelperDB.Mongo()
		#Récupere les poissons de wikipedia et les inserts dans la collection fishs
		mongo.RemoveInsert( 'fishs', scrap.ExtractFishs())
	def LoadWaterType(self):
		mongo = HelperDB.Mongo()
		mongo.Remove("waterTypes",{})
		mongo.Insert("waterTypes",{ "_id" : 1, "asciiName" : "etang", "variety" : [ "Étang", "étang", "etang", "etangs", "Ensemble de petits plans d'eau" ], "name" : "Étang" })
		mongo.Insert("waterTypes",{ "_id" : 2, "asciiName" : "lac", "variety" : [ "lac", "lacs" ], "name" : "Lac" })
		mongo.Insert("waterTypes",{ "_id" : 3, "asciiName" : "reservoir", "variety" : [ "reservoirs", "reservoir", "réservoir" ], "name" : "Réservoir" })
		mongo.Insert("waterTypes",{ "_id" : 4, "asciiName" : "ruisseau", "variety" : [ "ruisseau", "ruisseaux","Gaudre" ], "name" : "Ruisseau" })
		mongo.Insert("waterTypes",{ "_id" : 5, "asciiName" : "fleuve", "variety" : [ "fleuves", "fleuve" ], "name" : "Fleuve" })
		mongo.Insert("waterTypes",{ "_id" : 6, "asciiName" : "riviere", "variety" : [ "rivière", "rivières", "rivieres", "riviere", "Rivière" ], "name" : "Rivière" })
		mongo.Insert("waterTypes",{ "_id" : 7, "asciiName" : "canal", "variety" : [ "Canal ", "canal" ], "name" : "Canal" })
		mongo.Insert("waterTypes",{ "_id" : 9, "asciiName" : "impechable", "variety" : [ "Impêchable" ], "name" : "Impêchable" })
		mongo.Insert("waterTypes",{ "_id" : 10, "asciiName" : "inconnu", "variety" : [ "Inconnu" ], "name" : "Inconnu" })
	def LoadCategory(self):
		mongo = HelperDB.Mongo()
		mongo.Remove("categories",{})
		mongo.Insert("categories", {"_id" : 1, "name" : "1ère", "asciiName" : "1ere", "variety" : ["1ère", "1ere", "1 ere", "1 ère"] })
		mongo.Insert("categories", {"_id" : 2, "name" : "2ème", "asciiName" : "2eme", "variety" : ["2ème", "2nde","2 ème", "2eme"]})
		mongo.Insert("categories", {"_id" : 3, "name" : "mer", "asciiName" : "mer ", "variety" : ["mer "]})
	def LoadRule(self):
		mongo = HelperDB.Mongo()
		mongo.Remove("restrictions",{})
		mongo.Insert("restrictions", {"_id" : 1, "name" : "payant", "asciiName" : "payant", "variety" : ["payant"] })
		mongo.Insert("restrictions", {"_id" : 2, "name" : "timbre supplémentaire", "asciiName" : "timbre supplementaire", "variety" : ["timbre supplémentaire"] })
		mongo.Insert("restrictions", {"_id" : 3, "name" : "non réciprocitaire", "asciiName" : "non réciprocitaire", "variety" : ["non réciprocitaire"] })
		mongo.Insert("restrictions", {"_id" : 4, "name" : "peche interdite", "asciiName" : "peche interdite", "variety" : ["peche interdite"] })
		mongo.Insert("restrictions", {"_id" : 5, "name" : "réciprocitaire", "asciiName" : "reciprocitaire", "variety" : ["réciprocitaire"] })
	def LoadFishingTech(self):
		mongo = HelperDB.Mongo()
		mongo.Remove("fishingTech",{})
		mongo.Insert("fishingTech",{ "_id" : 1, "name":"Pêche au posé", "asciiName" : "", "variety" : ["Pêche au posé"],"description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 2, "name":"Pêche au coup","asciiName" : "", "variety" : ["Pêche au coup"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 3, "name":"Pêche à l'anglaise", "asciiName" : "", "variety" : ["Pêche à l'anglaise"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 4, "name":"Pêche à la mouche", "asciiName" : "", "variety" : ["Pêche à la mouche"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 5, "name":"Pêche au toc", "asciiName" : "", "variety" : ["Pêche au toc"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 6, "name":"Pêche à la bombette", "asciiName" : "", "variety" : ["Pêche à la bombette"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 7, "name":"Pêche à roder", "asciiName" : "", "variety" : ["Pêche à roder"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 8, "name":"Pêche en surfcasting", "asciiName" : "", "variety" : ["Pêche en surfcasting"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 9, "name":"Pêche au leurre souple", "asciiName" : "", "variety" : ["Pêche au leurre souple"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 10, "name":"Pêche à la traine", "asciiName" : "", "variety" : ["Pêche à la traine"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 11, "name":"Pêche à la cuillère", "asciiName" : "", "variety" : ["Pêche à la cuillère"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 12, "name":"Pêche à l'ondulante", "asciiName" : "", "variety" : ["Pêche à l'ondulante"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 13, "name":"Pêche au swimbaites", "asciiName" : "", "variety" : ["Pêche au swimbaites"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 14, "name":"Pêche au poissons nageur", "asciiName" : "", "variety" : ["Pêche au poissons nageur"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 15, "name":"Pêche au vif", "asciiName" : "", "variety" : ["Pêche au vif"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 15, "name":"Pêche au mort manié", "asciiName" : "", "variety" : ["Pêche au mort manié"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
		mongo.Insert("fishingTech",{ "_id" : 16, "name":"Pêche à soutenir","asciiName" : "", "variety" : ["Pêche à soutenir"], "description":"", "images":[], "url":[], "createdDate" :datetime.datetime.utcnow(), "updateDate":"", "subCategories":[] })
	def MergeDataArchigan(self):
		mongo = HelperDB.Mongo()
		count = 0
		collection = Collections.Collections()
		resultPlace = mongo.FindBatch("dataachigans", {}, 50)
		types = list(mongo.Find("waterTypes", {}))
		fishs = list(mongo.Find('fishs', {}))
		categories = list(mongo.Find('categories', {}))
		restrictions = list(mongo.Find('restrictions', {}))
		uniqid = mongo.GetSpotMaxUniqId("newSpots2")
		for er in resultPlace:
			fishsArchi = ' '.join(er["properties"]['fishs'])
			findExist = list(mongo.Find("newSpots2",{ 'geometry' : { '$geoIntersects': { '$geometry': er['geometry'] }}}))
			er["properties"]["fishs"] = [fishSingle["_id"] for fishSingle in fishs if fishSingle["asciiName"] in fishsArchi ]
			er['properties']['category'] = Helper.MatchData.FindValuesInArray(categories, er['properties']['category'], False)
			restrictArchi = ' '.join(er['properties']['rules']['restrictions'])
			er['properties']['rules']['restrictions'] = [restric["_id"] for restric in restrictions if restric["name"] in restrictArchi ]
			er["properties"]["description"] = ""
			contact = er["properties"]["contact"]
			_idContact =[]
			if contact["webSite"] or contact["adresse"] or contact["phoneNumber"] or contact["mail"] :
				_idContact = mongo.Insert("contacts", contact)
			er["properties"]["contacts"] = _idContact
			if len(findExist) > 0 and len(er["properties"]["fishs"])> 0:
				query = {"_id" : findExist[0]["_id"]}
				data = { '$set':
							{
								"properties.isPublic" : er["properties"]["isPublic"],
								"properties.fishs" : list(set(er["properties"]["fishs"] + findExist[0]["properties"]["fishs"])),
								"properties.category" : er["properties"]["category"],
								
								"properties.contacts" : er["properties"]["contacts"],
								"properties.rules.restrictions" : er['properties']['rules']['restrictions']
							}
				}
				count+=1
				pprint(list(set(er["properties"]["fishs"] + findExist[0]["properties"]["fishs"])))
				print(count) 
				mongo.Update("newSpots2", query, data)
			# else:
				# spot = collection.Spot()
				# spot["properties"]["uniqid"] = uniqid				
				# spot["properties"]["sourceId"] = str(er["properties"]["sourceId"])
				# spot["properties"]["source"] = "Achigan"
				# spot["properties"]["isPublic"] = er["properties"]["isPublic"]
				# spot["properties"]["fishs"] = er["properties"]["fishs"]
				# spot["properties"]["isValide"] = er["properties"]["isValide"]
				# spot["properties"]["category"] = er["properties"]["category"]
				# spot["properties"]["name"] = er["properties"]["name"]
				# spot["properties"]["toponyme"] = er["properties"]["name"]
				# spot["properties"]["contacts"] = er["properties"]["contacts"]
				# spot["properties"]["description"] = er["properties"]["description"]
				# spot["properties"]["enable"] = True
				# if er["properties"]["size"]:
					# spot["properties"]["size"].append(round(float(er["properties"]["size"]) * 100, 2))
				# spot['properties']['rules']['restrictions'] = er['properties']['rules']['restrictions']
				# spot['properties']['centroid']['coordinates'] = er['geometry']['coordinates']
				# spot["geometry"] = er['geometry']
				# findDepartement = mongo.Find("departements",{ 'geometry' : { '$geoIntersects': { '$geometry': er['geometry'] }}})
				# spot["properties"]["departements"] = [dep["_id"] for dep in findDepartement]
				# if spot["properties"]["name"] != None:
					# spot["properties"]["classification"] = Helper.MatchData.FindValuesInArray(types, er["properties"]["name"], True)
				# if len(spot["properties"]["classification"]) == 0:
					# typess = ''.join(er["properties"]["types"])
					# spot["properties"]["classification"] = Helper.MatchData.FindValuesInArray(types, typess, True)
				# if len(spot["properties"]["classification"]) == 0:
					# spot["properties"]["classification"] = [type["_id"] for type in types if type["name"] == "Inconnu"]
				# uniqid+=1
				# mongo.Insert("spots",spot)
class ETLV2:
	wikiFleuves = []
	wikiRivieres = []
	waterTypes = []
	coursEauWaterTypes= []
	coursEauRejectNames = []
	planEauWaterTypes = []
	planEauRejectNames = []
	def __init__(self):
		mongo = HelperDB.Mongo()
		print('Loading cache')
		self.coursEauRejectNames = ["Fossé", "bras", "bief", "Rigole", "Vallon","Ravin", "aqueduc"]
		self.planEauRejectNames = ["Fossé","graviere", "marais", "aqueduc"]
		self.planEauWaterTypes = list(mongo.Find("waterTypes", {"$and":[{"asciiName":{"$ne":"ruisseau"}},{"asciiName":{"$ne":"fleuve"}},{"asciiName":{"$ne":"riviere"}},{"asciiName":{"$ne":"canal"}}]}))
		self.coursEauWaterTypes = list(mongo.Find("waterTypes", {"$and":[{"asciiName":{"$ne":"lac"}},{"asciiName":{"$ne":"fleuve"}},{"asciiName":{"$ne":"riviere"}},{"asciiName":{"$ne":"etang"}},{"asciiName":{"$ne":"reservoir"}}]}))
		self.waterTypes = list(mongo.Find("waterTypes", {}))
		self.wikiFleuves = list(mongo.Find("WikiFleuves", {}))
		self.wikiRivieres = list(mongo.Find("WikiRivieres", {}))
		print('end loading cache')
	def FindDepartement(self, geometry):
		mongo = HelperDB.Mongo()
		departementFound = mongo.Find("departements",{ 'geometry' : { '$geoIntersects': { '$geometry': geometry }}})
		return [dep["_id"] for dep in departementFound]
	
	def FindClassificationByWikiStream(self, streams, asciiName, refSandre):
		streamFound = [stream for stream in streams if ("refSandre" in stream) and  stream["refSandre"] == refSandre]
		if len(streamFound) > 0:
			return {'found': True,
							'classification': [type["_id"] for type in self.waterTypes if type["asciiName"] == asciiName],
							'wikipediaUrl': streamFound[0]["url"]}
		else:
			return None
	
	def FindClassificationByName(self, name, waterTypesSpec, listToReject):
		if name == None: 
			return None
		if Helper.MatchData.StartWith(listToReject, name) == True:
			return {'classification': [type["_id"] for type in self.waterTypes if type["name"] == "Impêchable"],
							'enable':False
							}
		else:
			classification = Helper.MatchData.FindValuesInArray(waterTypesSpec, name, False)
			if len(classification) > 0:
				return {'classification': classification,
							'enable':True
							}
			else:
				return None
		
	def ImportSandreCoursEau(self):
		mongo = HelperDB.Mongo()
		print("Removing SandreCours")
		mongo.Remove("stagingSpots2", {"properties.source":"SandreCours"})
		self.ImportSandre("SandreCoursDeau2015", {}, self.TransformSandreCours)
		#"properties.Code_Hydrographique":"----0010"
		#"H1661100"
	def ImportSandrePlanEau(self):
		mongo = HelperDB.Mongo()
		print("Removing SandrePlanEau")
		mongo.Remove("stagingSpots2", {"properties.source":"SandrePlanEau"})
		self.ImportSandre("SandrePlanEau2015", {}, self.TransformPlanEau)
	def ImportSpot(self):
		mongo = HelperDB.Mongo()
		print("Removing newSpots2")
		mongo.Remove("newSpots2", {})
		print("end removing newSpots2")
		waters = mongo.FindBatch("stagingSpots2", {'$and':[{"properties.enable":True},{"properties.classification":{'$ne':9}}, {"properties.name":{'$nin':[""," "]} }]},  500)
		total = waters.count()
		data = []
		spotNbr = 0
		totalImported = 0
		for water in waters:
			spot = self.TransformExistingSpot(water)
			data.append(spot)
			spotNbr+=1
			if spotNbr == 500:
				mongo.BulkInsert('newSpots2', data)
				data = []
				totalImported += spotNbr
				print("["+str(totalImported) +"/" + str(total)+"]")
				spotNbr = 0
		if len(data) > 0:
			print("["+str(totalImported+len(data)) +"/" + str(total)+"]")
			mongo.BulkInsert( 'newSpots2', data)
			
	def ImportSandre(self, SandreCollection, query,  methodForTranform):
		mongo = HelperDB.Mongo()
		waters = mongo.FindBatch(SandreCollection, query, 500)
		total = waters.count()
		uniqid = mongo.GetSpotMaxUniqId("stagingSpots2")
		totalImported = 0
		spotNbr = 0
		data = []
		for water in waters:
			spot = methodForTranform(water)
			spot["properties"]["uniqid"] = uniqid
			data.append(spot)
			spotNbr += 1
			uniqid+=1
			if spotNbr == 500:
				mongo.BulkInsert('stagingSpots2', data)
				data = []
				totalImported += spotNbr
				print("["+str(totalImported) +"/" + str(total)+"]")
				spotNbr = 0
				
		if len(data) > 0:
			print("["+str(totalImported+len(data)) +"/" + str(total)+"]")
			mongo.BulkInsert( 'stagingSpots2', data)
			
	def TransformSandreCours(self, stream):
		spot = Collections.Collections.Spot()
		spot["properties"]["sourceId"] = str(stream["properties"]["Code_Hydrographique"])
		spot["properties"]["refSandre"] = str(stream["properties"]["Code_Hydrographique"])
		spot["properties"]["name"] = stream["properties"]["Toponyme"]
		spot["properties"]["toponyme"] = stream["properties"]["Toponyme"]
		spot["geometry"] = stream['geometry']
		spot["properties"]["source"] = "SandreCours" 
		
		spot["properties"]["departements"] = self.FindDepartement(spot["geometry"])
		findStream = self.FindClassificationByWikiStream(self.wikiFleuves, "fleuve", spot["properties"]["refSandre"])
	
		if findStream != None:
			spot["properties"]["classification"] = findStream["classification"]
			spot["properties"]["wikipediaUrl"] = findStream["wikipediaUrl"]
			
		if len(spot["properties"]["classification"]) == 0:
			findStream = self.FindClassificationByWikiStream(self.wikiRivieres, "riviere", spot["properties"]["refSandre"])
			if findStream != None:
				spot["properties"]["classification"] = findStream["classification"]
				spot["properties"]["wikipediaUrl"] = findStream["wikipediaUrl"]
		
		if len(spot["properties"]["classification"]) == 0 and spot["properties"]["name"] != None:
			found = self.FindClassificationByName(spot["properties"]["name"], self.coursEauWaterTypes, self.coursEauRejectNames)
			if found != None:
				spot["properties"]["classification"] = found["classification"]
				if found["enable"] == False:
					spot["properties"]["enable"] = False
					
		if len(spot["properties"]["classification"]) == 0:
			spot["properties"]["classification"] = [type["_id"] for type in self.waterTypes if type["name"] == "Inconnu"]	
		if spot["geometry"]["type"] == "MultiLineString":
			spot["properties"]["centroid"]["coordinates"] = [{"lon" : item[0], "lat" : item[1]} for sublist in spot["geometry"]["coordinates"] for item in sublist]
		else:
			spot["properties"]["centroid"]["coordinates"] = [{"lon" : coord[0], "lat" : coord[1]} for coord in spot["geometry"]["coordinates"] ]
		return spot;
	
	def TransformPlanEau(self, stream):
		calc = Helper.Calc()
		spot = Collections.Collections.Spot()
		spot["properties"]["sourceId"] = str(stream["properties"]["CdEniteHydrographique"])
		spot["properties"]["refSandre"] = str(stream["properties"]["CdEniteHydrographique"])
		spot["properties"]["name"] = str(stream["properties"]["NomEntiteHydrographique"])
		spot["properties"]["toponyme"] =  str(stream["properties"]["NomEntiteHydrographique"])
		spot["properties"]["source"] = "SandrePlanEau"
		spot["geometry"]= stream['geometry']
		spot["properties"]["departements"] = self.FindDepartement(spot["geometry"])
		
		if spot["geometry"]["type"] == "MultiPolygon":
			for coord in spot["geometry"]['coordinates'][0]:
					spot["properties"]["size"].append(round(calc.CalcPolygonAreaHectare(coord)/10000, 2))
					area = calc.CalcPolygonArea(coord, True)
					spot["properties"]["centroid"]["coordinates"].append(calc.CalcPolygonCentroid(coord, area))
		else:
			spot["properties"]["size"].append(round(calc.CalcPolygonAreaHectare(spot["geometry"]['coordinates'][0])/10000, 2))
			area = calc.CalcPolygonArea(spot["geometry"]['coordinates'][0], True)
			spot["properties"]["centroid"]["coordinates"].append(calc.CalcPolygonCentroid(spot["geometry"]['coordinates'][0], area))
		
		if len(spot["properties"]["classification"]) == 0 and spot["properties"]["name"] != None:
			found = self.FindClassificationByName(spot["properties"]["name"], self.planEauWaterTypes, self.planEauRejectNames)
			if found != None:
				spot["properties"]["classification"] = found["classification"]
				if found["enable"] == False:
					spot["properties"]["enable"] = False
		if len(spot["properties"]["classification"]) == 0:
			spot["properties"]["classification"] = [type["_id"] for type in self.waterTypes if type["name"] == "Inconnu"]
		return spot
	
	def TransformExistingSpot(self, stream):
		mongo = HelperDB.Mongo()
		spotExist = mongo.FindOne("spots", {"properties.sourceId":stream["properties"]["sourceId"]})
		if spotExist != None:
			spotback = copy.copy(stream);
			stream["_id"] = spotExist["_id"]
			stream["properties"] = spotExist["properties"]
			stream["properties"]["uniqid"] = spotback["properties"]["uniqid"]
			stream["properties"]["enable"] = spotback["properties"]["enable"]
			stream["properties"]["centroid"] = spotback["properties"]["centroid"]
			stream["properties"]["size"] = spotback["properties"]["size"]
			stream["properties"]["classification"] = spotback["properties"]["classification"]
		return stream

class AggregateSpot:
	def Achigan(self):
		mongo = HelperDB.Mongo()
		datas = mongo.Find("dataArchigan", {})
		total = datas.count()
		count = 0
		for data in datas:
			find = []
			info = ""
			if "geometry" in data and len(data["geometry"]) > 0:
				find = list(mongo.Find("newSpots2",{ 'geometry' : { '$geoIntersects': { '$geometry': data['geometry'] }}}))
				if len(find) == 0:
					find = list(mongo.Find("newSpots2",{"geometry":{ '$near': { '$geometry': data['geometry'], '$maxDistance':50 }}}))	

			if len(find) == 0:
				find = list(mongo.Find("newSpots2",{ 'properties.name' : data["name"]}))
			if 	len(find) > 0:
				count+=1
				if not "navigable" in data:
					data["navigable"] = False
				query = {"_id" : find[0]["_id"]}
				#"properties.navigable" : ["navigable"]
				spot = { '$set':
							{
								"properties.fishs" : list(set(data["fishs"] + find[0]["properties"]["fishs"])),
								"properties.category" : list(set(data["category"] + find[0]["properties"]["category"])),
								"properties.rules.restrictions" : list(set(find[0]["properties"]["rules"]["restrictions"] + data["restrictions"])),
								"properties.navigable" : data["navigable"]
							}
				}
			#print(info) 
				mongo.Update("newSpots2", query, spot)
		print(str(count) + "/" + str(total))
	def Gobage(self):
		print("")
	def Migrate(self):
		mysql = HelperDB.MySql()
		mongo = HelperDB.Mongo()
		spotBookmarks = list(mysql.FindRow("SELECT DISTINCT SpotId FROM SpotBookmarks"))
		spotBookmarks = [x[0] for x in spotBookmarks]
		comments = list(mysql.FindRow("SELECT DISTINCT SpotId FROM Comments"))
		comments = [x[0] for x in comments]
		spotContacts = list(mysql.FindRow("SELECT DISTINCT SpotId FROM SpotContacts"))
		spotContacts = [x[0] for x in spotContacts]
		spotIds = list(set(spotBookmarks + comments + spotContacts))
		
		count = 0
		for spotId in spotIds:
			spotFound = mongo.FindOne("newSpots2", {"_id":ObjectId(spotId)})
			if spotFound == None:
				count +=1
				spotFoundInSpot = mongo.FindOne("spots", {"_id":ObjectId(spotId)})
				if spotFoundInSpot != None:
					newSpotsFound = list(mongo.Find("newSpots2", {'$and': [{ 'geometry' : { '$geoIntersects': { '$geometry': spotFoundInSpot["geometry"] }}}, {"geometry.type": spotFoundInSpot["geometry"]["type"]}]}))
					if len(newSpotsFound) == 1:
						spotback = copy.copy(newSpotsFound[0]);
						newSpotsFound[0]["_id"] = spotFoundInSpot["_id"]
						newSpotsFound[0]["properties"] = spotFoundInSpot["properties"]
						newSpotsFound[0]["properties"]["uniqid"] = spotback["properties"]["uniqid"]
						newSpotsFound[0]["properties"]["enable"] = spotback["properties"]["enable"]
						newSpotsFound[0]["properties"]["centroid"] = spotback["properties"]["centroid"]
						newSpotsFound[0]["properties"]["size"] = spotback["properties"]["size"]
						mongo.Insert("newSpots2", newSpotsFound[0])
						mongo.Remove("newSpots2", {"_id":ObjectId(spotback["_id"])})
						print("MERGE SpotId "+" [()" + str(spotFoundInSpot["_id"]) +"):("+spotFoundInSpot["properties"]["name"]+")] found "+  " [()" + str(newSpotsFound[0]["_id"]) +"):("+newSpotsFound[0]["properties"]["name"]+")]")
					elif len(newSpotsFound) > 1:
						test =""
						for spot2 in newSpotsFound:
							test+= " [()" + str(spot2["_id"]) +"):("+spot2["properties"]["name"]+")]"
						print("SpotId MULTI  "+" [()" + str(spotFoundInSpot["_id"]) +"):("+spotFoundInSpot["properties"]["name"]+")]"+"found MULTI TIME "+ test)
					else:
						t = False
						if not "source" in spotFoundInSpot["properties"]:
							t = True
							mongo.Insert("newSpots2", spotFoundInSpot)
						if t == False:
							print("SpotId not found in newSpots2 "+spotId +" [()" + str(spotFoundInSpot["_id"]) +"):("+spotFoundInSpot["properties"]["name"]+")]")
				else:
					print("SPOT TO DELETE IN MYSQL"+spotId)
				
				#	pprint(spotfound)
		print(count)
# mongo = HelperDB.Mongo(); types = list(mongo.Find("waterTypes", {}));

#print(Helper.MatchData.FindValuesInArray(types, "mer ", True));

_etl = ETLV2()
#_etl.ImportSandreCoursEau()
#_etl.ImportSandrePlanEau()

#_etl.ImportSpot()
scrap = Scraping.Scraping()
#scrap.ExtractAchigan()

aggr = AggregateSpot()
#aggr.Achigan()
aggr.Migrate()
mysql = HelperDB.MySql()
# pprint(mysql.Find("SELECT * FROM Fishes"))


    


#etl.LoadWaterType();
#etl.initRefCollections()

#mongo = HelperDB.Mongo()

#[type["_id"] for type in types if type["name"] == "Inconnu"]
#print(Helper.MatchData.StartWith(["Fossé", "bras", "bief ", "Rigole "], "bief Ruisseau du Fossé Lac"))

# importStream.SandrePlanEau();
#etl.MergeDataArchigan();
# scrap.ExtractGobages()
#importStream.SandreCours();

#importStream.InsertSpots()
#scrap.ExtractGobages()

#scrap.ExtractFishs() 
#scrap.ExtractRiviere() 
#scrap.ExtractFleuve2() 
#scrap.ExtractGobages()

